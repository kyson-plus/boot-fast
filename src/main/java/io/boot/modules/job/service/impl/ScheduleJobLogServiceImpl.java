

package io.boot.modules.job.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.Query;
import io.boot.modules.job.dao.ScheduleJobLogDao;
import io.boot.modules.job.entity.ScheduleJobLogEntity;
import io.boot.modules.job.service.ScheduleJobLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogDao, ScheduleJobLogEntity> implements ScheduleJobLogService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String jobId = (String)params.get("jobId");

		IPage<ScheduleJobLogEntity> page = this.page(
			new Query<ScheduleJobLogEntity>().getPage(params),
			new QueryWrapper<ScheduleJobLogEntity>()
					.select("job_id")
					.like(StringUtils.isNotBlank(jobId),"job_id", jobId).orderByDesc("create_time")
				.select("log_id")
		);
		List<Long> ids= page.getRecords().stream().map(ScheduleJobLogEntity::getLogId).collect(Collectors.toList());
//		List<ScheduleJobLogEntity> xsList=this.baseMapper.selectBatchIds(ids);
			List<ScheduleJobLogEntity> xsList=this.baseMapper.selectList(new QueryWrapper<ScheduleJobLogEntity>().in("log_id",ids).orderByDesc("log_id"));
		page.setRecords(xsList);
		return new PageUtils(page);
	}

}
