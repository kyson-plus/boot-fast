

package io.boot.modules.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.job.entity.ScheduleJobLogEntity;

import java.util.Map;

/**
 * 定时任务日志
 *
 * @author kyson
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(Map<String, Object> params);

}
