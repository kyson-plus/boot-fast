

package io.boot.modules.job.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.job.entity.ScheduleJobLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 定时任务日志
 *
 * @author kyson
 */
@Mapper
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {

}
