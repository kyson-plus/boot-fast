

package io.boot.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.boot.common.annotation.BaseData;
import io.boot.common.annotation.DataScope;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;
import io.boot.common.validator.ValidatorUtils;
import io.boot.modules.sys.entity.SysDictEntity;
import io.boot.modules.sys.service.SysDictService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 数据字典
 *
 * @author kyson
 */
@RestController
@RequestMapping("sys/dict")
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:dict:list")
    @DataScope
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDictService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    @RequiresPermissions("sys:dict:info")
    public R info(@PathVariable("id") Long id){
        SysDictEntity dict = sysDictService.getById(id);

        return R.ok().put("dict", dict);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    @RequiresPermissions("sys:dict:save")
    @BaseData("save")
    public R save(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);

        sysDictService.save(dict);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @RequiresPermissions("sys:dict:update")
    @BaseData("update")
    public R update(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);
        sysDictService.updateById(dict);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @RequiresPermissions("sys:dict:delete")
    public R delete(@RequestBody Long[] ids){
        sysDictService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 根据type查询数据
     */
    @GetMapping(value = "/type/{type}")
    public R byTypeGetDictEntity(@PathVariable("type") String type){
        //校验类型
        List<SysDictEntity> dictList=sysDictService.list(new QueryWrapper<SysDictEntity>().eq("type",type));
        return R.ok().put("dictList",dictList);
    }

    /**
     * 根据type,value查询对象name
     */
    @GetMapping(value = "/typeValue")
    public R byTypeGetDictEntity(@RequestParam("type")String type,@RequestParam("value")Integer value){
        //校验类型
        SysDictEntity sysDictEntity=sysDictService.getOne(new QueryWrapper<SysDictEntity>().eq("type",type).eq("value",value));
        return R.ok().put("sysDictEntity",sysDictEntity);
    }


}
