package io.boot.modules.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.boot.common.annotation.BaseData;
import io.boot.common.annotation.DataScope;
import io.boot.modules.sys.entity.SysDeptEntity;
import io.boot.modules.sys.entity.SysMenuEntity;
import io.boot.modules.sys.service.SysDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;



/**
 * 部门表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-06 16:58:51
 */
@RestController
@RequestMapping("sys/dept")
public class SysDeptController {
    @Autowired
    private SysDeptService sysDeptService;

    /**
     * 列表
     */
    @GetMapping("/list")
//    @RequiresPermissions("sys:dept:list")
    public R list(){
        List<SysDeptEntity> deptEntityList = sysDeptService.list(new QueryWrapper<SysDeptEntity>().orderByAsc("order_num"));
        deptEntityList.remove(0);
        for(SysDeptEntity sysDeptEntity : deptEntityList){
            SysDeptEntity parentSysDeptEntity = sysDeptService.getById(sysDeptEntity.getParentId());
            if(parentSysDeptEntity != null){
                sysDeptEntity.setParentName(parentSysDeptEntity.getDeptName());
            }
        }
        return R.ok().put("list",deptEntityList);
    }


    /**
     * 选择部门(添加、修改部门)
     */
    @GetMapping("/select")
    public R select(){
        //查询列表数据
        List<SysDeptEntity> deptList = sysDeptService.queryNotButtonList();
        for(SysDeptEntity sysDeptEntity : deptList){
            SysDeptEntity parentSysDeptEntity = sysDeptService.getById(sysDeptEntity.getParentId());
            if(parentSysDeptEntity != null){
                sysDeptEntity.setParentName(parentSysDeptEntity.getDeptName());
            }
        }
        return R.ok().put("deptList", deptList);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{deptId}")
//    @RequiresPermissions("sys:dept:info")
    public R info(@PathVariable("deptId") Long deptId){
		SysDeptEntity sysDept = sysDeptService.getById(deptId);

        return R.ok().put("sysDept", sysDept);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
//    @RequiresPermissions("sys:dept:save")
    @BaseData("save")
    public R save(@RequestBody SysDeptEntity sysDept){
        SysDeptEntity sysDeptParent = sysDeptService.getById(sysDept.getParentId());
        sysDept.setAncestors(sysDeptParent.getAncestors()+","+sysDeptParent.getDeptId());
		sysDeptService.save(sysDept);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
//    @RequiresPermissions("sys:dept:update")
    @BaseData("update")
    public R update(@RequestBody SysDeptEntity sysDept){
        SysDeptEntity sysDeptParent = sysDeptService.getById(sysDept.getParentId());
        sysDept.setAncestors(sysDeptParent.getAncestors()+","+sysDeptParent.getDeptId());
		sysDeptService.updateById(sysDept);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
//    @RequiresPermissions("sys:dept:delete")
    public R delete(@RequestBody Long[] deptIds){
		sysDeptService.removeByIds(Arrays.asList(deptIds));

        return R.ok();
    }

}
