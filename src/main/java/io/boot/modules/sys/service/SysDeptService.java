package io.boot.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.sys.entity.SysDeptEntity;

import java.util.List;
import java.util.Map;

/**
 * 部门表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-06 16:58:51
 */
public interface SysDeptService extends IService<SysDeptEntity> {

    List<SysDeptEntity> queryNotButtonList();

    List<Long> byDeptGetDeptChild(Long deptId);
}

