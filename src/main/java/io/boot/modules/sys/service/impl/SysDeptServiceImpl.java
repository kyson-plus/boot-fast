package io.boot.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.boot.modules.sys.dao.SysDeptDao;
import io.boot.modules.sys.entity.SysDeptEntity;
import io.boot.modules.sys.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("sysDeptService")
public class SysDeptServiceImpl extends ServiceImpl<SysDeptDao, SysDeptEntity> implements SysDeptService {

    @Override
    public List<SysDeptEntity> queryNotButtonList() {
        return baseMapper.queryNotButtonList();
    }

    @Override
    public List<Long> byDeptGetDeptChild(Long deptId) {
        return baseMapper.byDeptGetDeptChild(deptId);
    }

}
