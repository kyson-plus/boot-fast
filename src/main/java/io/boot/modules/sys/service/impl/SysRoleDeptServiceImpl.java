package io.boot.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.boot.modules.sys.dao.SysRoleDeptDao;
import io.boot.modules.sys.entity.SysRoleDeptEntity;
import io.boot.modules.sys.service.SysRoleDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;


@Service("sysRoleDeptService")
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptDao, SysRoleDeptEntity> implements SysRoleDeptService {



    @Override
    public List<Long> byRoleGetDeptList(Long roleId) {
        return baseMapper.byRoleGetDeptList(roleId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(Long roleId, List<Long> deptIds) {
        //先删除角色与菜单关系
        deleteBatch(new Long[]{roleId});

        if(deptIds==null){
            return ;
        }

        //保存角色与菜单关系
        for(Long deptId : deptIds){
            SysRoleDeptEntity sysRoleDeptEntity=new SysRoleDeptEntity();
            sysRoleDeptEntity.setRoleId(roleId);
            sysRoleDeptEntity.setDeptId(deptId);
            this.save(sysRoleDeptEntity);
        }
    }


    private Integer deleteBatch(Long[] roleId) {
       return baseMapper.deleteBatch(roleId);
    }

}
