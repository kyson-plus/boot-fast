package io.boot.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.sys.entity.SysRoleDeptEntity;

import java.util.List;
import java.util.Map;

/**
 * 角色和部门关联表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-06 16:58:51
 */
public interface SysRoleDeptService extends IService<SysRoleDeptEntity> {


    List<Long> byRoleGetDeptList(Long roleId);

    void saveOrUpdate(Long roleId, List<Long> deptIds);

}

