

package io.boot.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.sys.entity.SysLogEntity;

import java.util.Map;


/**
 * 系统日志
 *
 * @author kyson
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
