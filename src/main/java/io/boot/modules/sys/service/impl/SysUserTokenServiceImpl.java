

package io.boot.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.boot.common.utils.R;
import io.boot.modules.sys.dao.SysUserTokenDao;
import io.boot.modules.sys.entity.SysUserTokenEntity;
import io.boot.modules.sys.oauth2.TokenGenerator;
import io.boot.modules.sys.service.SysConfigService;
import io.boot.modules.sys.service.SysUserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service("sysUserTokenService")
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenDao, SysUserTokenEntity> implements SysUserTokenService {
	//12小时后过期
	private final static int EXPIRE = 3600 * 12;

	@Autowired
	private SysConfigService sysConfigService;


	@Override
	public R createToken(long userId) {
		String value=sysConfigService.getValue("signOn");
		//生成一个token
		String token="";
		//判断是否生成过token
		SysUserTokenEntity tokenEntity = this.getById(userId);
		if(value==null||"true".equals(value)) {
			token= TokenGenerator.generateValue();
		}else{
		   if(tokenEntity==null){
			   token= TokenGenerator.generateValue();
		   }else{
		   	   token=tokenEntity.getToken();
		   }
		}

		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);


		if(tokenEntity == null){
			tokenEntity = new SysUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);
			//保存token
			this.save(tokenEntity);
		}else{

			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//更新token
			this.updateById(tokenEntity);
		}
		R r = R.ok().put("token", token).put("expire", EXPIRE);

		return r;
	}

	@Override
	public void logout(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();

		//修改token
		SysUserTokenEntity tokenEntity = new SysUserTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		this.updateById(tokenEntity);
	}
}
