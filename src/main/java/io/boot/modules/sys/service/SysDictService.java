

package io.boot.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.sys.entity.SysDictEntity;

import java.util.Map;

/**
 * 数据字典
 *
 * @author kyson
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

