

package io.boot.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.boot.common.utils.BaseEntity;
import io.boot.common.utils.SpringContextUtils;
import io.boot.common.validator.group.AddGroup;
import io.boot.common.validator.group.UpdateGroup;
import io.boot.modules.msg.entity.MessageEntity;
import io.boot.modules.msg.service.MessageService;
import io.boot.modules.sys.service.SysDeptService;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 系统用户
 *
 * @author kyson
 */
@Data
@TableName("sys_user")
public class SysUserEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@TableId
	private Long userId;

	/**
	 * 用户名
	 */
	@NotBlank(message="用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String username;

	/**
	 * 密码
	 */
	@NotBlank(message="密码不能为空", groups = AddGroup.class)
	private String password;

	/**
	 * 盐
	 */
	private String salt;

	/**
	 * 邮箱
	 */
	@NotBlank(message="邮箱不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Email(message="邮箱格式不正确", groups = {AddGroup.class, UpdateGroup.class})
	private String email;

	/**
	 * 手机号
	 */
	private String mobile;

	/**
	 * 状态  0：禁用   1：正常
	 */
	private Integer status;

	/**
	 * 角色ID列表
	 */
	@TableField(exist=false)
	private List<Long> roleIdList;


	/**
	 * 部门
	 */
	private Long deptId;

	/**
	 * 部门名称
	 */
	@TableField(exist=false)
	private String deptName;

	public String getDeptName() {
		SysDeptService sysDeptService=SpringContextUtils.getBean("sysDeptService", SysDeptService.class);
		SysDeptEntity sysDeptEntity = sysDeptService.getById(this.deptId);
		return sysDeptEntity!=null?sysDeptEntity.getDeptName():"";
	}

	/**
	 * 未读消息数量
	 */
	@TableField(exist=false)
	private Integer msgNoRead;

	public Integer getMsgNoRead() {
		MessageService messageService=SpringContextUtils.getBean("messageService", MessageService.class);
		Integer num = messageService.count(new QueryWrapper<MessageEntity>().eq("create_by",userId).eq("status",0));
		return num;
	}
}
