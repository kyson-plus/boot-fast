

package io.boot.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.boot.common.utils.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 角色
 *
 * @author kyson
 */
@Data
@TableName("sys_role")
public class SysRoleEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 角色ID
	 */
	@TableId
	private Long roleId;

	/**
	 * 角色名称
	 */
	@NotBlank(message="角色名称不能为空")
	private String roleName;


	/**
	 * 备注
	 */
	private String remark;

	@TableField(exist=false)
	private List<Long> menuIdList;

	/** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限 5:仅本人数据权限） */
	private String dataScope;

	/** 部门组（数据权限） */
	@TableField(exist=false)
	private List<Long> deptIdList;

}
