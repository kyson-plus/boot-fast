

package io.boot.modules.sys.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.sys.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 *
 * @author kyson
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {

}
