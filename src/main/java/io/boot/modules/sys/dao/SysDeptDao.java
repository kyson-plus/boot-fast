package io.boot.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.sys.entity.SysDeptEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 部门表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-06 16:58:51
 */
@Mapper
public interface SysDeptDao extends BaseMapper<SysDeptEntity> {

    List<SysDeptEntity> queryNotButtonList();

    List<Long> byDeptGetDeptChild(Long deptId);
}
