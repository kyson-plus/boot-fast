package io.boot.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.sys.entity.SysRoleDeptEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色和部门关联表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-06 16:58:51
 */
@Mapper
public interface SysRoleDeptDao extends BaseMapper<SysRoleDeptEntity> {

    List<Long> byRoleGetDeptList(@Param("roleId") Long roleId);

    Integer deleteBatch(Long[] roleId);
}
