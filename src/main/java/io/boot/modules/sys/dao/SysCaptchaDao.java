

package io.boot.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.sys.entity.SysCaptchaEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 验证码
 *
 * @author kyson
 */
@Mapper
public interface SysCaptchaDao extends BaseMapper<SysCaptchaEntity> {

}
