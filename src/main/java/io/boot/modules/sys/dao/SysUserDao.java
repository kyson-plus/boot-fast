

package io.boot.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统用户
 *
 * @author kyson
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {

	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);

	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);

	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

    List<Long> byDeptGetUserList(@Param("deptId") Long deptId);

	List<Long> byAncestorsAndDeptId(@Param("deptId")Long deptId);
}
