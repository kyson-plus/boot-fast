package io.boot.modules.msg.event;

import io.boot.modules.msg.service.MessageService;
import io.boot.modules.msg.service.MsgInfoService;
import io.boot.modules.msg.service.impl.WebSocketMsgServer;
import io.boot.modules.sys.controller.AbstractController;
import io.boot.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 登录监听事件
 * @author lyf
 *
 */
@Component
public class LoginListener extends AbstractController {
	private static final long serialVersionUID = -8448722764259810497L;

	@Autowired
	private MessageService messageService;

	@Autowired
	private MsgInfoService msgInfoService;

	@Autowired
	private SysUserService sysUserService;

	@Autowired
	private WebSocketMsgServer webSocketMsgServer;

	/**
	 * 完全可以不用写监听器 只是加一个新的技术，后面用到可以参考
	 * 可以通过注解的方式 解决问题
	 * @param event
	 */
	@EventListener
	@Async
	public void onApplicationEvent(Loginevent event) {
		//现获取最后一次消息，如果没有，就获取注册信息！！！

	}
}
