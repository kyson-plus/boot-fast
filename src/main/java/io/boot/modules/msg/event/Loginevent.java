package io.boot.modules.msg.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author kyson
 * @date 2020/05/14 13:40
 */
public class Loginevent  extends ApplicationEvent {
    private static final long serialVersionUID = 1L;

    private Long userId ;


    public Loginevent(Object source,Long userId) {
        super(source);
        this.userId=userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
