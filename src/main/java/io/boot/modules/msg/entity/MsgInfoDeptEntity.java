package io.boot.modules.msg.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.boot.common.utils.BaseEntity;
import lombok.Data;

/**
 * 消息主体跟部门中间表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@Data
@TableName("msg_info_dept")
public class MsgInfoDeptEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long id;
	/**
	 * 消息详情id
	 */
	private Long msgInfoId;
	/**
	 * 部门id
	 */
	private Long deptId;

}
