package io.boot.modules.msg.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.boot.common.utils.BaseEntity;
import lombok.Data;

/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@Data
@TableName("msg_info")
public class MsgInfoEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long msgInfoId;
	/**
	 * 主题
	 */
	private String title;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 消息类型 1.部门数据权限 2.部门及以下数据权限 3.自定义数据权限
	 */
	private Integer msgType;
	/**
	 * 推送方式 1.立即推送 2.定时推送
	 */
	private Integer msgPushType;
	/**
	 * 推送时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date msgPushTime;
	/**
	 * 有效时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date msgExpireTime;

	/** 部门组（数据权限） */
	@TableField(exist=false)
	private List<Long> deptIdList;

	/**
	 * 时间格式化
	 */
	@TableField(exist=false)
	private Long msgPushTimeS;

	public Long getMsgPushTimeS() {
		return msgPushTime.getTime();
	}
}
