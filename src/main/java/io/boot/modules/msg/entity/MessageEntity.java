package io.boot.modules.msg.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.boot.common.utils.BaseEntity;
import io.boot.common.utils.SpringContextUtils;
import io.boot.modules.msg.service.MsgInfoService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@Data
@TableName("message")
//@NoArgsConstructor
//@AllArgsConstructor
public class MessageEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long msgId;
	/**
	 * 发送人
	 */
	private String sender;
	/**
	 * 接收人
	 */
	private String receiver;
	/**
	 * 消息详情内容id
	 */
	private Long msgInfoId;
	/**
	 * 状态 (0 ：未读 1：已读)
	 */
	private Integer status;

	@TableField(exist = false)
	private MsgInfoEntity msgInfoEntity;



	public MsgInfoEntity getMsgInfoEntity() {
		MsgInfoService msgInfoService= (MsgInfoService) SpringContextUtils.getBean("msgInfoService");
		return msgInfoService.getById(this.msgInfoId);
	}
}
