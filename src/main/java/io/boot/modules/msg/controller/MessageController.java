package io.boot.modules.msg.controller;

import java.util.Arrays;
import java.util.Map;

import io.boot.common.annotation.BaseData;
import io.boot.common.annotation.DataScope;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.boot.modules.msg.entity.MessageEntity;
import io.boot.modules.msg.service.MessageService;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;



/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@RestController
@RequestMapping("msg/message")
public class MessageController {
    @Autowired
    private MessageService messageService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("msg:message:list")
    @DataScope
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = messageService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{msgId}")
//    @RequiresPermissions("msg:message:info")
    public R info(@PathVariable("msgId") Long msgId){
		MessageEntity message = messageService.getById(msgId);

        return R.ok().put("message", message);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @BaseData("save")
//    @RequiresPermissions("msg:message:save")
    public R save(@RequestBody MessageEntity message){
		messageService.save(message);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @BaseData("update")
//    @RequiresPermissions("msg:message:update")
    public R update(@RequestBody MessageEntity message){
		messageService.updateById(message);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("msg:message:delete")
    public R delete(@RequestBody Long[] msgIds){
		messageService.removeByIds(Arrays.asList(msgIds));
        return R.ok();
    }

}
