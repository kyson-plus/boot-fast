package io.boot.modules.msg.controller;

import java.util.Arrays;
import java.util.Map;

import io.boot.common.annotation.BaseData;
import io.boot.common.annotation.DataScope;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.boot.modules.msg.entity.MsgInfoDeptEntity;
import io.boot.modules.msg.service.MsgInfoDeptService;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;



/**
 * 消息主体跟部门中间表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@RestController
@RequestMapping("msg/msginfodept")
public class MsgInfoDeptController {
    @Autowired
    private MsgInfoDeptService msgInfoDeptService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("msg:msginfodept:list")
    @DataScope
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = msgInfoDeptService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("msg:msginfodept:info")
    public R info(@PathVariable("id") Long id){
		MsgInfoDeptEntity msgInfoDept = msgInfoDeptService.getById(id);

        return R.ok().put("msgInfoDept", msgInfoDept);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @BaseData("save")
    @RequiresPermissions("msg:msginfodept:save")
    public R save(@RequestBody MsgInfoDeptEntity msgInfoDept){
		msgInfoDeptService.save(msgInfoDept);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @BaseData("update")
    @RequiresPermissions("msg:msginfodept:update")
    public R update(@RequestBody MsgInfoDeptEntity msgInfoDept){
		msgInfoDeptService.updateById(msgInfoDept);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("msg:msginfodept:delete")
    public R delete(@RequestBody Long[] ids){
		msgInfoDeptService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
