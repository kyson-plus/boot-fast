package io.boot.modules.msg.controller;

import java.util.*;

import io.boot.common.annotation.BaseData;
import io.boot.common.annotation.DataScope;
import io.boot.common.utils.QuartzUtil;
import io.boot.modules.msg.service.MsgInfoDeptService;
import io.boot.modules.msg.service.impl.MessageSend;
import io.boot.modules.sys.controller.AbstractController;
import io.boot.modules.sys.service.SysDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.quartz.JobDataMap;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.boot.modules.msg.entity.MsgInfoEntity;
import io.boot.modules.msg.service.MsgInfoService;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;



/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@RestController
@RequestMapping("msg/msginfo")
public class MsgInfoController extends AbstractController {
    @Autowired
    private MsgInfoService msgInfoService;
    @Autowired
    private MsgInfoDeptService msgInfoDeptService;
    @Autowired
    private QuartzUtil quartzUtil;


    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("msg:msginfo:list")
    @DataScope
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = msgInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{msgInfoId}")
//    @RequiresPermissions("msg:msginfo:info")
    public R info(@PathVariable("msgInfoId") Long msgInfoId){
		MsgInfoEntity msgInfo = msgInfoService.getById(msgInfoId);
        //查询信息跟部门的关系
        List<Long> deptIdList = msgInfoDeptService.byMsgInfoGetDeptList(msgInfoId);
        msgInfo.setDeptIdList(deptIdList);

        return R.ok().put("msgInfo", msgInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @BaseData("save")
//    @RequiresPermissions("msg:msginfo:save")
    @Transactional
    public R save(@RequestBody MsgInfoEntity msgInfo) throws SchedulerException {
        if(msgInfo.getMsgPushTime()==null){
            msgInfo.setMsgPushTime(new Date());
        }
		msgInfoService.save(msgInfo);
        msgInfoDeptService.saveOrUpdate(msgInfo,getUser());
        if(msgInfo.getMsgPushType()==2){
            JobDataMap dataMap = new JobDataMap();
            dataMap.put("msgInfo",msgInfo);
            dataMap.put("user",getUser());
            quartzUtil.addJob("msgInfo"+msgInfo.getMsgInfoId(),msgInfo.getMsgInfoId().toString(), MessageSend.class, msgInfo.getMsgPushTime(), dataMap);
        }else{
            msgInfoService.sendMsg(msgInfo,getUser());
        }
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @BaseData("update")
    @Transactional
//    @RequiresPermissions("msg:msginfo:update")
    public R update(@RequestBody MsgInfoEntity msgInfo) throws SchedulerException {
		msgInfoService.update(msgInfo,getUser());

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("msg:msginfo:delete")
    public R delete(@RequestBody Long[] msgInfoIds){
		msgInfoService.removeByIds(Arrays.asList(msgInfoIds));

        return R.ok();
    }

}
