package io.boot.modules.msg.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author kyson
 * @date 2020/04/26 09:20
 */
@Data
public class WebSocketMessage {
    /**
     * 发送者ID
     */
    private String senderId;

    /**
     * 接受者ID, 如果为0, 则发送给所有人
     */
    private String receiverId;

    /**
     * 会话内容
     */
    private String messageContent;

    /**
     * 发送时间
     */
    private Date sendTime;
}
