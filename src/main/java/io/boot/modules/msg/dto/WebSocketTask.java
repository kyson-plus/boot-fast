package io.boot.modules.msg.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author kyson
 * @date 2020/05/14 09:45
 *
 * websocket失败任务
 *
 */

@Data
public class WebSocketTask implements Serializable {

    /**任务id*/
    private String taskId;
    /**消息*/
    private String msg;
    /**发送时间*/
    private long startTime;
    /**用户id*/
    private String userId;
    /**组id*/
    private String groupId;

    /**任务编号*/
    private Integer taskNum;


    public static void main(String[] args) {

        ResWsTask resWsTask = new ResWsTask();
//        resWsTask.setType(0);
        WebSocketTask task = new WebSocketTask();
        task.setTaskId(String.valueOf(new Date().getTime()));
        task.setMsg("测试消息");
        task.setStartTime(System.currentTimeMillis());
        task.setUserId(String.valueOf(new Date().getTime()));
        task.setGroupId(String.valueOf(new Date().getTime()));
        task.setTaskNum(100001);
        resWsTask.setTask(task);
    }


}
