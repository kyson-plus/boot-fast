package io.boot.modules.msg.dto;

import lombok.Data;

import javax.websocket.Session;
import java.io.Serializable;

/**
 * @author kyson
 * @date 2020/05/14 09:43
 * websocketsession实体类
 */

@Data
public class WebsocketSessionDto implements Serializable {

    /**
     * sessionid
     */
    private String sessionId;
    /**
     * websocket的session
     */
    private Session session;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 心跳时间
     */
    private long heartbeatTime;
    /**
     * 组id
     */
    private String groupId;
    /**
     * 最后链接时间
     */
    private long lastConTime;

}
