package io.boot.modules.msg.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * websocket返回值
 * @author kyson
 * @date 2020/05/14 09:44
 */
@Data
public class ResWsTask implements Serializable {

    /**type默认0 普通消息   1,心跳*/
    private int  methodType = 0;
    /**返回结果 */
    private WebSocketTask task;
}
