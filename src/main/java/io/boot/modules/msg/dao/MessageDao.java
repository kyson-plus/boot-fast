package io.boot.modules.msg.dao;

import io.boot.modules.msg.entity.MessageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;

/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@Mapper
public interface MessageDao extends BaseMapper<MessageEntity> {

    Date getLastDate(Long userId);
}
