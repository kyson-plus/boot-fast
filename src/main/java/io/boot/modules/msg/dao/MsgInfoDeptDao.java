package io.boot.modules.msg.dao;

import io.boot.modules.msg.entity.MsgInfoDeptEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 消息主体跟部门中间表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@Mapper
public interface MsgInfoDeptDao extends BaseMapper<MsgInfoDeptEntity> {


    int deleteBatch(Long[] longs);

    List<Long> byMsgInfoGetDeptList(Long msgInfoId);
}
