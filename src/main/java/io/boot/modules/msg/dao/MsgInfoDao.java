package io.boot.modules.msg.dao;

import io.boot.modules.msg.entity.MsgInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
@Mapper
public interface MsgInfoDao extends BaseMapper<MsgInfoEntity> {

    List<MsgInfoEntity> getMsgInfoList(Date date, Long deptId);

    List<MsgInfoEntity> getNoStartJob();
}
