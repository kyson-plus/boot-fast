package io.boot.modules.msg.service.impl;

import com.alibaba.fastjson.JSON;
import io.boot.common.utils.QuartzUtil;
import io.boot.modules.msg.entity.MessageEntity;
import io.boot.modules.msg.service.MessageService;
import io.boot.modules.msg.service.MsgInfoDeptService;
import io.boot.modules.sys.entity.SysUserEntity;
import io.boot.modules.sys.service.SysUserService;
import org.quartz.JobDataMap;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.Query;

import io.boot.modules.msg.dao.MsgInfoDao;
import io.boot.modules.msg.entity.MsgInfoEntity;
import io.boot.modules.msg.service.MsgInfoService;


@Service("msgInfoService")
public class MsgInfoServiceImpl extends ServiceImpl<MsgInfoDao, MsgInfoEntity> implements MsgInfoService {

    @Autowired
    private MsgInfoDeptService msgInfoDeptService;
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private WebSocketMsgServer webSocketMsgServer;
    @Autowired
    private MessageService messageService;
    @Autowired
    private QuartzUtil quartzUtil;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MsgInfoEntity> page = this.page(
                new Query<MsgInfoEntity>().getPage(params),
                new QueryWrapper<MsgInfoEntity>()
                .in((List<Long>) params.get("list")!=null,"create_by",(List<Long>) params.get("list"))
        );

        return new PageUtils(page);
    }

    @Override
    public void update(MsgInfoEntity msgInfo,SysUserEntity sysUserEntity) throws SchedulerException {
        this.updateById(msgInfo);
        //更新角色与权限的关系
        msgInfoDeptService.saveOrUpdate(msgInfo,sysUserEntity);
        if(msgInfo.getMsgPushType()==2){
            quartzUtil.removeJob("msgInfo"+msgInfo.getMsgInfoId(),msgInfo.getMsgInfoId().toString());
            JobDataMap dataMap = new JobDataMap();
            dataMap.put("msgInfo",msgInfo);
            dataMap.put("user",sysUserEntity);
            quartzUtil.addJob("msgInfo"+msgInfo.getMsgInfoId(),msgInfo.getMsgInfoId().toString(), MessageSend.class, msgInfo.getMsgPushTime(), dataMap);
        }
    }

    @Override
    public List<MsgInfoEntity> getMsgInfoList(Date date, Long deptId) {
        return baseMapper.getMsgInfoList(date,deptId);
    }



    @Override
    public void sendMsg(MsgInfoEntity msgInfo, SysUserEntity user) throws SchedulerException {
        List<Long> userList=new ArrayList<>();
        if(msgInfo.getMsgType()==1){
            userList = sysUserService.byDeptGetUserList(user.getDeptId());
        }else if(msgInfo.getMsgType()==2){
            userList = sysUserService.byAncestorsAndDeptId(user.getDeptId());
        }else if(msgInfo.getMsgType()==3){
            List<Long> deptIdList = msgInfo.getDeptIdList();
            for (Long deptId:deptIdList){
                userList.addAll(sysUserService.byDeptGetUserList(deptId));
            }
        }
        for (Long userId:userList) {
            SysUserEntity sysUser = sysUserService.getById(userId);
            Map<String,Object> msgE=new HashMap<>();
            msgE.put("title",msgInfo.getTitle());
            msgE.put("content",msgInfo.getContent());
            Boolean flag=webSocketMsgServer.sendInfo(JSON.toJSONString(msgE),String.valueOf(userId));
            if(flag) {
                MessageEntity messageEntity = new MessageEntity();
                messageEntity.setSender(user.getUsername());
                messageEntity.setReceiver(sysUser.getUsername());
                messageEntity.setMsgInfoId(msgInfo.getMsgInfoId());
                messageEntity.setStatus(0);
                messageEntity.setCreateBy(userId);
                messageEntity.setCreateTime(new Date());
                messageService.save(messageEntity);
            }
        }
    }

    @Override
    public List<MsgInfoEntity> getNoStartJob() {
        return baseMapper.getNoStartJob();
    }


}
