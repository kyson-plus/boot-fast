package io.boot.modules.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.msg.entity.MsgInfoDeptEntity;
import io.boot.modules.msg.entity.MsgInfoEntity;
import io.boot.modules.sys.entity.SysUserEntity;

import java.util.List;
import java.util.Map;

/**
 * 消息主体跟部门中间表
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
public interface MsgInfoDeptService extends IService<MsgInfoDeptEntity> {

    PageUtils queryPage(Map<String, Object> params);



    void saveOrUpdate(MsgInfoEntity msgInfo, SysUserEntity user);

    List<Long> byMsgInfoGetDeptList(Long msgInfoId);
}

