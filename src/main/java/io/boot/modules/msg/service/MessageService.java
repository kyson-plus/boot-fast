package io.boot.modules.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.msg.entity.MessageEntity;

import java.util.Date;
import java.util.Map;

/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
public interface MessageService extends IService<MessageEntity> {

    PageUtils queryPage(Map<String, Object> params);

    Date getLastDate(Long userId);

    void sendMsg(Long userId);
}

