package io.boot.modules.msg.service.impl;

import io.boot.common.utils.QuartzUtil;
import io.boot.common.utils.SpringContextUtils;
import io.boot.modules.msg.entity.MsgInfoEntity;
import io.boot.modules.msg.service.MsgInfoService;
import io.boot.modules.sys.entity.SysUserEntity;
import lombok.SneakyThrows;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

/**
 * @author kyson
 * @date 2020/05/14 19:03
 */
@Service
public class MessageSend extends QuartzJobBean {




    @SneakyThrows
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        MsgInfoService msgInfoService= (MsgInfoService) SpringContextUtils.getBean("msgInfoService");
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        MsgInfoEntity msgInfoEntity= (MsgInfoEntity) dataMap.get("msgInfo");
        SysUserEntity sysUserEntity= (SysUserEntity) dataMap.get("user");
        msgInfoService.sendMsg(msgInfoEntity,sysUserEntity);

        QuartzUtil quartzUtil=(QuartzUtil)SpringContextUtils.getBean("quartzUtil");
        quartzUtil.removeJob("msgInfo"+msgInfoEntity.getMsgInfoId(),msgInfoEntity.getMsgInfoId().toString());
    }
}
