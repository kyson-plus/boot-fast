package io.boot.modules.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.msg.entity.MsgInfoEntity;
import io.boot.modules.sys.entity.SysUserEntity;
import org.quartz.SchedulerException;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author kyson
 * @email 570@qq.com
 * @date 2020-05-13 16:58:15
 */
public interface MsgInfoService extends IService<MsgInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void update(MsgInfoEntity msgInfo,SysUserEntity sysUserEntity) throws SchedulerException;

    List<MsgInfoEntity> getMsgInfoList(Date date,Long deptId);

    void sendMsg(MsgInfoEntity msgInfo, SysUserEntity user) throws SchedulerException;

    List<MsgInfoEntity> getNoStartJob();
}

