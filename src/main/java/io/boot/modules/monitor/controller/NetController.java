package io.boot.modules.monitor.controller;

import io.boot.common.utils.Constant;
import io.boot.common.utils.R;
import io.boot.modules.monitor.dto.NetVO;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author kyson
 * @date 2020/04/25 19:00
 */
@RestController
@RequestMapping("/monitor/net")
public class NetController {
    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 查询流量最近的50次
     */
    @GetMapping("/list")
    public R list(){
        // 网络流量：查询最近
        List<NetVO> list = redisTemplate.opsForList().range(Constant.MONITOR_NET, Constant.ZERO_LONG, (Constant.MONITOR_NET_GET - 1));
        return R.ok().put("list", list);
    }

}
