package io.boot.modules.monitor.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author kyson
 * @date 2020/04/24 22:40
 * 流量
 */
@Data
public class NetVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("当前时间")
    private String currentTime;
    @ApiModelProperty("当前接收包裹数")
    private Long currentRxPackets;
    @ApiModelProperty("当前发送包裹数")
    private Long currentTxPackets;
    @ApiModelProperty("当前接收字节数")
    private Long currentRxBytes;
    @ApiModelProperty("当前发送字节数")
    private Long currentTxBytes;

    @ApiModelProperty("接收总包裹数")
    private Long rxPackets;
    @ApiModelProperty("发送总包裹数")
    private Long txPackets;
    @ApiModelProperty("接收总字节数")
    private Long rxBytes;
    @ApiModelProperty("发送总字节数")
    private Long txBytes;
}
