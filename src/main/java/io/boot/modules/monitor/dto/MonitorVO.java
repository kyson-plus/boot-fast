package io.boot.modules.monitor.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author kyson
 * @date 2020/04/24 22:37
 * 监控
 */

@Data
public class MonitorVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("cpu")
    private CpuVO cpu;
    @ApiModelProperty("内存")
    private MemoryVO memory;
    @ApiModelProperty("硬盘")
    private DiskVO disk;
    @ApiModelProperty("网络流量")
    private List<NetVO> netList;
}
