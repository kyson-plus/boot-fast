package io.boot.modules.monitor.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author kyson
 * @date 2020/04/24 22:39
 * 内存
 */
@Data
public class MemoryVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("内存总量")
    private String memory;
    @ApiModelProperty("内存使用量")
    private String memRam;
    @ApiModelProperty("使用中")
    private String memUsed;
    @ApiModelProperty("可用")
    private String memFrees;
    @ApiModelProperty("内存使用率")
    private String memoryUsage;

}
