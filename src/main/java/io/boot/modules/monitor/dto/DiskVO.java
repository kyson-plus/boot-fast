package io.boot.modules.monitor.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author kyson
 * @date 2020/04/24 22:39
 * 内存
 */
@Data
public class DiskVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("总容量")
    private String total;
    @ApiModelProperty("剩余大小")
    private String free;
    @ApiModelProperty("可用大小")
    private String avail;
    @ApiModelProperty("已使用大小")
    private String used;
    @ApiModelProperty("资源利用率")
    private String usePercent;
}
