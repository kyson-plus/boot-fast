package io.boot.modules.monitor.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author kyson
 * @date 2020/04/24 22:38
 * cpu
 */
@Data
public class CpuVO implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("CPU用户使用率")
    private String user;
    @ApiModelProperty("CPU系统使用率")
    private String sys;
    @ApiModelProperty("CPU当前等待率")
    private String wait;
    @ApiModelProperty("CPU当前错误率")
    private String nice;
    @ApiModelProperty("CPU当前空闲率")
    private String idle;
    @ApiModelProperty("CPU总的使用率")
    private String combined;
}
