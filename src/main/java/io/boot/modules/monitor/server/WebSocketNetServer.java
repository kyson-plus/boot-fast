package io.boot.modules.monitor.server;

import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author kyson
 * @date 2020/04/26 09:18
 */
@ServerEndpoint("/ws/net/{sid}")
@Component
public class WebSocketNetServer {
    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static CopyOnWriteArraySet<WebSocketNetServer> webSocketSet = new CopyOnWriteArraySet<>();
    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    /**
     * 接收sid
     */
    private String sid="";




    /**
     * 连接建立成功调用的方法*/
    @OnOpen
    public void onOpen(Session session,@PathParam("sid") String sid) {
        this.session = session;
        this.sid=sid;
        boolean flag=true;
        for (WebSocketNetServer item : webSocketSet) {
            if(item.sid.equals(sid)){
                flag=false;
                break;
            }
        }
        if(flag){
            webSocketSet.add(this);
        }
        addOnlineCount(); //在线数加1
        System.out.println("有新连接加入！当前在线人数为" + getOnlineCount());
        sendMessage("有新连接加入！当前在线人数为" + getOnlineCount());
//        onMessage("有新连接加入！当前在线人数为" + getOnlineCount(), session);
    }
    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this); //从set中删除
        subOnlineCount(); //在线数减1
        System.out.println("有一连接关闭！当前在线人数为" + getOnlineCount());
    }


    /**
     * 收到客户端消息后调用的方法
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("来自客户端的消息:" + message);
        //定点发送
        for (WebSocketNetServer item : webSocketSet) {
            if(item.sid.equals(sid)) {
                item.sendMessage(message);
            }
        }
    }

    /**
     * 异常推送
     * @param session session
     * @param error 异常
     */
    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");
        error.printStackTrace();
    }

    /**
     * 发送信息
     * @param message 信息
     * @throws IOException IO异常
     */
    private void sendMessage(String message)  {
        try {
            this.session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //this.session.getAsyncRemote().sendText(message);
    }

    /**
     * 群发自定义消息(sid ==0 就是群发)
     * */
    public static void sendInfo(String message,@PathParam("sid") String sid){
        for (WebSocketNetServer item : webSocketSet) {
            try {
                if(sid.equals("0")) {
                    item.sendMessage(message);
                }else if(item.sid.equals(sid)){
                    item.sendMessage(message);
                }
            } catch (Exception e) {
                continue;
            }
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketNetServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketNetServer.onlineCount--;
    }
}
