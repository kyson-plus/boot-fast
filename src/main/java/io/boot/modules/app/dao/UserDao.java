

package io.boot.modules.app.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.app.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户
 *
 * @author kyson
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {

}
