

package io.boot.modules.activiti.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.activiti.entity.ActLeaveEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 请加流程
 *
 * @author kyson
 */
@Mapper
public interface ActLeaveDao extends BaseMapper<ActLeaveEntity> {

}
