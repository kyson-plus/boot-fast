package io.boot.modules.activiti.dto;

/**
 * @author kyson
 * @date 2020/04/20 11:45
 */

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 待签任务
 */
@Data
public class Pending implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    private String id;

    /**
     * 流程名称
     */
    private String processName;

    /**
     * 流程版本
     */
    private String processVersion;

    /**
     * 待办人名称
     */
    private String upUser;

    /**
     * 待办任务名称
      */
    private String upName;

    /**
     * 通知时间
     */
    private Date createTime;

    /**
     * 任务状态
     */
    private Boolean type;


    /**
     * 任务人id
     */
    private String assignee;


    /**
     * 提审人
     */
    private String referee;

    /**
     * 流程Id
     */
    private String instanceId;









}
