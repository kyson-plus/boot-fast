package io.boot.modules.activiti.dto;

/**
 * @author kyson
 * @date 2020/04/20 11:45
 */

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 已办任务
 */
@Data
public class Done implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    private String id;

    /**
     * 已处理流程名称
     */
    private String name;


    /**
     * 提审人
     */
    private String referee;

    /**
     * 处理人名称
     */
    private String upName;

    /**
     * 通知时间
     */
    private String createTime;


    /**
     * 处理时间
     */
    private String endTime;

    /**
     * 流程Id
     */
    private String instanceId;











}
