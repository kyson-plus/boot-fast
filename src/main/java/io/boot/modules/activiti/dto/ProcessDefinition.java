package io.boot.modules.activiti.dto;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ProcessDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

//    @Excel(name = "流程名称")
    private String name;

//    @Excel(name = "流程KEY")
    private String key;

//    @Excel(name = "流程版本")
    private int version;

//    @Excel(name = "所属分类")
    private String category;

//    @Excel(name = "流程描述")
    private String description;

    private String deploymentId;

//    @Excel(name = "部署时间", dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deploymentTime;

    /**
     * 状态
     */
    private Boolean  type;

    /**
     * 流程定义Id
     */
    private String definitId;

}
