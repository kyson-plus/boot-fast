

package io.boot.modules.activiti.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.boot.common.utils.BaseEntity;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据字典
 *
 * @author kyson
 */
@Data
@TableName("act_leave")
public class ActLeaveEntity extends BaseEntity  implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;

	/**
	 * 请假类型
	 */
	@NotBlank(message="请假类型不能为空")
	private String type;
	/**
	 * 标题
	 */
	@NotBlank(message="标题不能为空")
	private String title;
	/**
	 * 原因
	 */
	@NotBlank(message="原因不能为空")
	private String reason;
	/**
	 * 开始时间
	 */
//	@NotBlank(message="开始时间不能为空")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	/**
	 *结束时间
	 */
//	@NotBlank(message="结束时间不能为空")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	/**
	 *请假时长
	 */
//	@NotBlank(message="结束时间不能为空")
	private String totalTime;

	/**
	 * 启动的流程id
	 */
	private String instanceId;


	/**
	 * 状态
	 */
	@TableField(exist = false)
	private Boolean status;


	/**
	 * 当前步骤
	 */
	@TableField(exist = false)
	private String nowTaskName;




}
