package io.boot.modules.activiti.controller;

import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;
import io.boot.modules.activiti.service.UpComingService;
import io.boot.modules.sys.controller.AbstractController;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 待办任务
 *
 * @author kyson
 */
@RestController
@RequestMapping("activiti/upcoming")
public class UpComingController extends AbstractController {
    @Autowired
    private UpComingService upComingService;
    @Autowired
    private TaskService taskService;



    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = upComingService.queryPage(params,getUser().getUsername());
        return R.ok().put("page", page);
    }

    /**
     * 处理完成
     */
    @PostMapping("/dealwith")
    public R dealwith(@RequestBody Long taskId){
        taskService.complete(taskId.toString()); //与正在执行的任务管理相关的Service
        return R.ok();
    }

    /**
     * 归还任务
     */
    @PostMapping("/returnTask")
    public R returnTask(@RequestBody Map<String, Object> params){
        Task task = taskService.createTaskQuery().taskId(params.get("taskId").toString()).singleResult();
        if(task.getAssignee()!=null){
            //任务归还
            taskService.setAssignee(params.get("taskId").toString(),null);
        }
        return R.ok();
    }

    //归还任务



}
