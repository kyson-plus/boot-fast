package io.boot.modules.activiti.controller;

import io.boot.common.annotation.DataScope;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;
import io.boot.modules.activiti.service.DoneService;
import io.boot.modules.activiti.service.PendingService;
import io.boot.modules.sys.controller.AbstractController;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 待签任务
 *
 * @author kyson
 */
@RestController
@RequestMapping("activiti/pending")
public class PendingController extends AbstractController {
    @Autowired
    private PendingService pendingService;
    @Autowired
    private TaskService taskService;


    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pendingService.queryPage(params,getUser().getUsername());
        return R.ok().put("page", page);
    }


    /**
     * 拾取任务
     */
    @PostMapping("/pickup")
    public R pickup(@RequestBody Map<String, Object> params){
        Task task = taskService.createTaskQuery().taskId(params.get("taskId").toString()).singleResult();
        if(task.getAssignee()==null){
            //任务拾取
            taskService.claim(params.get("taskId").toString(), getUser().getUsername()); //与正在执行的任务管理相关的Service
        }else{
            return R.error("任务已被其他人拾取了!");
        }
        return R.ok();
    }



}
