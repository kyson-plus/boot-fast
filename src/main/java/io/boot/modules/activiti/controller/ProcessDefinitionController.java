package io.boot.modules.activiti.controller;

import io.boot.common.utils.PageUtils;
import io.boot.common.utils.R;
import io.boot.modules.activiti.service.impl.ProcessDefinitionService;
import io.boot.modules.sys.controller.AbstractController;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 定义流程
 */
@RestController
@RequestMapping("/activiti/definition")
public class ProcessDefinitionController extends AbstractController {


    @Autowired
    private ProcessDefinitionService processDefinitionService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private RepositoryService repositoryService;


    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = processDefinitionService.queryPage(params);
        return R.ok().put("page", page);
    }




    /**
     * 部署流程定义
     */
//    @RequiresPermissions("process:definition:upload")
//    @PostMapping("/upload")
//    @ResponseBody
//    public AjaxResult upload(@RequestParam("processDefinition") MultipartFile file) {
//        try {
//            if (!file.isEmpty()) {
//                String extensionName = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.') + 1);
//                if (!"bpmn".equalsIgnoreCase(extensionName)
//                        && !"zip".equalsIgnoreCase(extensionName)
//                        && !"bar".equalsIgnoreCase(extensionName)) {
//                    return error("流程定义文件仅支持 bpmn, zip 和 bar 格式！");
//                }
//                // p.s. 此时 FileUploadUtils.upload() 返回字符串 fileName 前缀为 Constants.RESOURCE_PREFIX，需剔除
//                // 详见: FileUploadUtils.getPathFileName(...)
//                String fileName = FileUploadUtils.upload(Global.getProfile() + "/processDefiniton", file);
//                if (StringUtils.isNotBlank(fileName)) {
//                    String realFilePath = Global.getProfile() + fileName.substring(Constants.RESOURCE_PREFIX.length());
//                    processDefinitionService.deployProcessDefinition(realFilePath);
//                    return success();
//                }
//            }
//            return error("不允许上传空文件！");
//        }
//        catch (Exception e) {
//            log.error("上传流程定义文件失败！", e);
//            return error(e.getMessage());
//        }
//    }

    @PostMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
        for (Long id:ids) {
            processDefinitionService.deleteProcessDeploymentByIds(id.toString());
        }
        return R.ok();
    }

    /**
     * 激活 挂起
     * @param params
     * @return
     */
    @PostMapping("/hangactive")
    public R hangactive(@RequestBody Map<String, Object> params) {
        if(params.get("type").toString().equals("true")){
            //激活
            repositoryService.activateProcessDefinitionById(params.get("id").toString(),true,null);
        }else{
            //挂起
            repositoryService.suspendProcessDefinitionById(params.get("id").toString(),true,null);
        }
        return R.ok();
    }

}
