package io.boot.modules.activiti.service;

import io.boot.common.utils.PageUtils;

import java.util.Map;

/**
 * @author kyson
 * @date 2020/04/21 17:03
 */
public interface PendingService {
    PageUtils queryPage(Map<String, Object> params, String userName);
}
