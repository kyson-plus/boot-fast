package io.boot.modules.activiti.service;

import io.boot.common.utils.PageUtils;

import java.util.Map;

/**
 * @author kyson
 * @date 2020/04/20 11:33
 */
public interface UpComingService {
    PageUtils queryPage(Map<String, Object> params, String userName);
}
