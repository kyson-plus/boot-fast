package io.boot.modules.activiti.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.boot.common.utils.DateUtils;
import io.boot.common.utils.PageUtils;
import io.boot.modules.activiti.dto.Done;
import io.boot.modules.activiti.entity.ActLeaveEntity;
import io.boot.modules.activiti.service.ActLeaveService;
import io.boot.modules.activiti.service.DoneService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author kyson
 * @date 2020/04/20 11:34
 */
@Service("DoneServiceImpl")
public class DoneServiceImpl implements DoneService {

    @Autowired
    private HistoryService historyService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ActLeaveService actLeaveService;


    @Override
    public PageUtils queryPage(Map<String, Object> params, String userName) {
        Integer page=Integer.valueOf(params.get("page").toString());
        Integer limit=Integer.valueOf(params.get("limit").toString());
        List<HistoricTaskInstance> historicTaskInstancelist = historyService.createHistoricTaskInstanceQuery()
                .finished()
                .orderByHistoricTaskInstanceEndTime()
                .desc()
                .taskAssignee(userName)
                .listPage((page - 1) * limit, limit);
        List<Done> doneList=new ArrayList<>();
        for (HistoricTaskInstance hi:historicTaskInstancelist) {
            Done done=new Done();
            done.setId(hi.getId());
            done.setName(hi.getName());
            done.setCreateTime(DateUtils.format(hi.getCreateTime(),DateUtils.DATE_TIME_PATTERN));
            done.setEndTime(DateUtils.format(hi.getEndTime(),DateUtils.DATE_TIME_PATTERN));
//            ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(hi.getProcessInstanceId()).includeProcessVariables().singleResult();
//            if(processInstance!=null) {
//                done.setReferee(processInstance.getProcessVariables().get("createBy") != null ? processInstance.getProcessVariables().get("createBy").toString() : "");
//            }else{
//                done.setReferee("");
//            }
            done.setInstanceId(hi.getProcessInstanceId());
            done.setUpName(hi.getAssignee());
            doneList.add(done);
        }
        PageUtils pages =new PageUtils(doneList,Integer.valueOf(historyService.createHistoricTaskInstanceQuery().count()+""),page,limit);
        return pages;
    }
}
