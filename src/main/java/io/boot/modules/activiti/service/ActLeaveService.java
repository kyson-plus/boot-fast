package io.boot.modules.activiti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.boot.common.utils.PageUtils;
import io.boot.modules.activiti.entity.ActLeaveEntity;

import java.util.Map;

/**
 * @author kyson
 * @date 2020/04/17 11:50
 */
public interface ActLeaveService extends IService<ActLeaveEntity> {

    PageUtils queryPage(Map<String, Object> params, Long userId);
}
