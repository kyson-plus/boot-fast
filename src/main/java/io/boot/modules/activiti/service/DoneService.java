package io.boot.modules.activiti.service;

import io.boot.common.utils.PageUtils;

import java.util.Map;

/**
 * @author kyson
 * @date 2020/04/20 16:07
 */
public interface DoneService {
    PageUtils queryPage(Map<String, Object> params, String userId);
}
