package io.boot.modules.activiti.main;

import io.boot.modules.sys.entity.SysDeptEntity;
import io.boot.modules.sys.entity.SysUserEntity;
import io.boot.modules.sys.service.SysDeptService;
import io.boot.modules.sys.service.SysUserService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.persistence.entity.GroupEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author kyson
 * @date 2020/04/22 22:00
 */
@Component
public class CustomGroupEntityManager extends GroupEntityManager {
    @Lazy
    @Autowired
    private SysUserService userService;

    @Lazy
    @Autowired
    private SysDeptService sysDeptService;


    @Override
    public List<Group> findGroupsByUser(String userName) {
        SysUserEntity sysUserEntity=userService.queryByUserName(userName);
        SysDeptEntity sysDeptEntity=sysDeptService.getById(sysUserEntity.getDeptId());
        Group  gs= ActivitiUserUtil.toActivitiGroup(sysUserEntity.getUserId(),sysDeptEntity.getNickName());
        List<Group> gsList=new ArrayList<>();
        gsList.add(gs);
        return gsList;
    }

    @Override
    public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public List<Group> findGroupsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
        throw new RuntimeException("not implement method.");
    }

}

