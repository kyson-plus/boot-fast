package io.boot.modules.activiti.main;

import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kyson
 * @date 2020/04/23 09:31
 */
@Configuration
public class ActivitiConfiguration implements ProcessEngineConfigurationConfigurer {

    @Autowired
    private CustomUserEntityManagerFactory customUserEntityManagerFactory;

    @Autowired
    private CustomGroupEntityManagerFactory customGroupEntityManagerFactory;

    @Override
    public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
        // TODO Auto-generated method stub
        processEngineConfiguration.setDatabaseSchemaUpdate("none");// none true
        processEngineConfiguration.setDatabaseType("mysql");

        // 流程图字体
        processEngineConfiguration.setActivityFontName("宋体");
        processEngineConfiguration.setAnnotationFontName("宋体");
        processEngineConfiguration.setLabelFontName("宋体");

        processEngineConfiguration.setJpaHandleTransaction(false);
        processEngineConfiguration.setJpaCloseEntityManager(false);
        //

        processEngineConfiguration.setJobExecutorActivate(false);
        processEngineConfiguration.setAsyncExecutorEnabled(false);
        //自定义用户和组
        List<SessionFactory> customSessionFactories = new ArrayList<>();
        customSessionFactories.add(customUserEntityManagerFactory);
        customSessionFactories.add(customGroupEntityManagerFactory);
        processEngineConfiguration.setCustomSessionFactories(customSessionFactories);

    }
}

