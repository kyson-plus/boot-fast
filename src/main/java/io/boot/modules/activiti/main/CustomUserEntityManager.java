package io.boot.modules.activiti.main;

import io.boot.modules.sys.entity.SysDeptEntity;
import io.boot.modules.sys.entity.SysUserEntity;
import io.boot.modules.sys.service.SysDeptService;
import io.boot.modules.sys.service.SysUserService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.persistence.entity.IdentityInfoEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author kyson
 * @date 2020/04/22 21:29
 */
@Component
public class CustomUserEntityManager extends UserEntityManager{

    @Autowired
    @Lazy
    private SysUserService userService;

    @Lazy
    @Autowired
    private SysDeptService sysDeptService;

    @Override
    public UserEntity findUserById(String userId) {
        SysUserEntity user = getUser(userId);
        return ActivitiUserUtil.toActivitiUser(user);
    }

    @Override
    public List<Group> findGroupsByUser(String userName) {
        if (userName == null) {
            return null;
        }
        SysUserEntity sysUserEntity=userService.queryByUserName(userName);
        SysDeptEntity sysDeptEntity=sysDeptService.getById(sysUserEntity.getDeptId());
        Group  gs= ActivitiUserUtil.toActivitiGroup(sysUserEntity.getUserId(),sysDeptEntity.getNickName());
        List<Group> gsList=new ArrayList<>();
        gsList.add(gs);
        return gsList;
    }

    @Override
    public List<org.activiti.engine.identity.User> findUserByQueryCriteria(UserQueryImpl query, Page page) {
        SysUserEntity user = getUser(query.getId());
        List<org.activiti.engine.identity.User> list = new ArrayList<>();
        list.add(ActivitiUserUtil.toActivitiUser(user));
        return list;
    }

    private SysUserEntity getUser(String userId) {
        SysUserEntity sysUserEntity = userService.getById(userId);
        return sysUserEntity;
    }

    @Override
    public long findUserCountByQueryCriteria(UserQueryImpl query) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public IdentityInfoEntity findUserInfoByUserIdAndKey(String userId, String key) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public List<String> findUserInfoKeysByUserIdAndType(String userId, String type) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public List<org.activiti.engine.identity.User> findPotentialStarterUsers(String proceDefId) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public List<org.activiti.engine.identity.User> findUsersByNativeQuery(Map<String, Object> parameterMap,
                                                                          int firstResult, int maxResults) {
        throw new RuntimeException("not implement method.");
    }

    @Override
    public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
        throw new RuntimeException("not implement method.");
    }

}
