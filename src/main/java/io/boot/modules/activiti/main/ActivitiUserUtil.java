package io.boot.modules.activiti.main;

import io.boot.modules.sys.entity.SysUserEntity;
import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kyson
 * @date 2020/04/22 21:41
 */
public class ActivitiUserUtil {
    public static UserEntity toActivitiUser(SysUserEntity bUser){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(bUser.getUserId().toString());
        userEntity.setFirstName(bUser.getUsername());
        userEntity.setLastName(bUser.getUsername());
        userEntity.setPassword(bUser.getPassword());
        userEntity.setEmail(bUser.getEmail());
        userEntity.setRevision(1);
        return userEntity;
    }

    public static GroupEntity toActivitiGroup(Long enterpriseBasicId, String code){
        GroupEntity groupEntity = new GroupEntity();
        groupEntity.setRevision(1);
        groupEntity.setType("assignment");
        groupEntity.setId(code);
        return groupEntity;
    }

    public static List<Group> toActivitiGroups(Long enterpriseBasicId, List<String> deptCodeList){
        List<Group> groups = new ArrayList<>();
        for (String code : deptCodeList) {
            GroupEntity groupEntity = toActivitiGroup(enterpriseBasicId,code);
            groups.add(groupEntity);
        }
        return groups;
    }
}
