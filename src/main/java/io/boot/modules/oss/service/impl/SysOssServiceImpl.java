

package io.boot.modules.oss.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.boot.common.utils.PageUtils;
import io.boot.common.utils.Query;
import io.boot.modules.oss.dao.SysOssDao;
import io.boot.modules.oss.entity.SysOssEntity;
import io.boot.modules.oss.service.SysOssService;
import io.boot.modules.sys.entity.SysUserEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("sysOssService")
public class SysOssServiceImpl extends ServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<SysOssEntity> page = this.page(
			new Query<SysOssEntity>().getPage(params),
				new QueryWrapper<SysOssEntity>()
						.in((List<Long>) params.get("list")!=null,"create_by",(List<Long>) params.get("list"))

		);

		return new PageUtils(page);
	}

}
