

package io.boot.modules.oss.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.boot.common.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;


/**
 * 文件上传
 *
 * @author kyson
 */
@Data
@TableName("sys_oss")
public class SysOssEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;
	//URL地址
	private String url;
	/**
	 * 类型
	 */
	private String type;

	/**
	 * 实际图片地址
	 */
	private String urlPath;

}
