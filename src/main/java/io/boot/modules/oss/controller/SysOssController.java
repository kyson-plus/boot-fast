

package io.boot.modules.oss.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import io.boot.common.annotation.BaseData;
import io.boot.common.annotation.DataScope;
import io.boot.common.exception.RRException;
import io.boot.common.utils.*;
import io.boot.modules.oss.cloud.CloudStorageConfig;
import io.boot.modules.oss.entity.SysOssEntity;
import io.boot.modules.oss.service.SysOssService;
import io.boot.modules.sys.controller.AbstractController;
import io.boot.modules.sys.service.SysConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 文件上传
 *
 * @author kyson
 */
@RestController
@RequestMapping("sys/oss")
public class SysOssController extends AbstractController {
	@Autowired
	private SysOssService sysOssService;
    @Autowired
    private SysConfigService sysConfigService;
	@Value("${file.maxSize}")
	private long maxSize;
	@Value("${file.path}")
	private String filepath;

    private final static String KEY = ConfigConstant.CLOUD_STORAGE_CONFIG_KEY;

	/**
	 * 列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:oss:all")
	@DataScope
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysOssService.queryPage(params);

		return R.ok().put("page", page);
	}


    /**
     * 云存储配置信息
     */
    @GetMapping("/config")
    @RequiresPermissions("sys:oss:all")
    public R config(){
        CloudStorageConfig config = sysConfigService.getConfigObject(KEY, CloudStorageConfig.class);

        return R.ok().put("config", config);
    }



	/**
	 * 上传文件
	 */
	@PostMapping("/upload")
	@RequiresPermissions("sys:oss:all")
	public R upload(@RequestParam("file") MultipartFile multipartFile) throws Exception {
		if (multipartFile.isEmpty()) {
			throw new RRException("上传文件不能为空");
		}
		FileUtils.checkSize(maxSize, multipartFile.getSize());
		String suffix = FileUtils.getExtensionName(multipartFile.getOriginalFilename());
		String type = FileUtils.getFileType(suffix);
		File file = FileUtils.upload(multipartFile, filepath+type+"/");
		if(ObjectUtil.isNull(file)){
			return R.error("上传失败");
		}
		try {
			//保存文件信息
			SysOssEntity ossEntity = new SysOssEntity();
			ossEntity.setUrl(file.getPath());
			ossEntity.setUrlPath(("/file/"+filepath+type+"/"+file.getName()).replace(filepath,""));
			ossEntity.setType(type);
			ossEntity.setCreateTime(new Date());
			ossEntity.setCreateBy(getUserId());
			sysOssService.save(ossEntity);
			return R.ok().put("url",ossEntity.getUrlPath());
		}catch (Exception e){
			FileUtil.del(file);
			throw e;
		}

	}


	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@RequiresPermissions("sys:oss:all")
	public R delete(@RequestBody Long[] ids){
		sysOssService.removeByIds(Arrays.asList(ids));

		return R.ok();
	}

}
