

package io.boot.modules.oss.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.boot.modules.oss.entity.SysOssEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 *
 * @author kyson
 */
@Mapper
public interface SysOssDao extends BaseMapper<SysOssEntity> {

}
