package io.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //允许使用socketJs方式访问，访问点为webSocket，允许跨域
        //在网页上我们就可以通过这个链接
        //ws://127.0.0.1:8585/webSocket来和服务器的WebSocket连接
        registry.addEndpoint("/ws").setAllowedOrigins("*");
    }


}
