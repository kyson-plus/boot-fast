

package io.boot.common.annotation;

import java.lang.annotation.*;

/**
 * BaseEntity实体更新
 *
 * @author kyson
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BaseData {

	String value() default "";
}
