

package io.boot.common.annotation;

import java.lang.annotation.*;

/**
 * 数据权限控制
 *
 * @author kyson
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScope {

	String value() default "";
}
