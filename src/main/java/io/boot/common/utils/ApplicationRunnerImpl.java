package io.boot.common.utils;

import io.boot.modules.msg.entity.MsgInfoEntity;
import io.boot.modules.msg.service.MsgInfoService;
import io.boot.modules.msg.service.impl.MessageSend;
import io.boot.modules.sys.service.SysUserService;
import org.quartz.JobDataMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author kyson
 * @date 2020/05/18 18:12
 */
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {

    @Autowired
    private QuartzUtil quartzUtil;
    @Autowired
    private MsgInfoService msgInfoService;
    @Autowired
    private SysUserService sysUserService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<MsgInfoEntity> msgInfoEntityList=msgInfoService.getNoStartJob();
        for (MsgInfoEntity msgInfo:msgInfoEntityList) {
            JobDataMap dataMap = new JobDataMap();
            dataMap.put("msgInfo",msgInfo);
            dataMap.put("user",sysUserService.getById(msgInfo.getCreateBy()));
            quartzUtil.addJob("msgInfo"+msgInfo.getMsgInfoId(),msgInfo.getMsgInfoId().toString(), MessageSend.class, new Date(), dataMap);
        }
        System.out.println("消息后台启动成功！！！");
    }
}
