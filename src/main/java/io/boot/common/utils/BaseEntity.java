package io.boot.common.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.boot.modules.sys.controller.AbstractController;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Entity基类
 *
 * @author ruoyi
 */
@Data
public class BaseEntity extends AbstractController implements Serializable
{
    private static final long serialVersionUID = 1L;


    /** 创建者 */
    private Long createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /** 更新者 */
    private Long updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;


    /**
     * 数据权限自动填充 list 数据 根据 createBy 查询所有数据
     */
    @TableField(exist=false)
    private List<Long> createByList;


    /**
     * 是否有操作权限 也就是说是否是自己创建的数据
     */
    @TableField(exist=false)
    private Boolean hasAction;


    public Boolean getHasAction() {
        if(this.createBy==getUserId()){
            return true;
        }else{
            return false;
        }
    }
}
