//package io.boot.common.utils;
//
//import com.baidu.aip.bodyanalysis.AipBodyAnalysis;
//import org.json.JSONObject;
//import org.springframework.util.Base64Utils;
//import org.springframework.util.StringUtils;
//
//import java.awt.*;
//import java.io.*;
//import java.util.Base64;
//import java.util.HashMap;
//
///**
// * @author kyson
// * @date 2020/03/17 12:01
// */
//public class Sample {
//    //设置APPID/AK/SK
//    public static final String APP_ID = "18886600";//自己的App ID
//    public static final String API_KEY = "2Odve7YHkudTnW5jjQRcCy1Y";//自己的 Api Key
//    public static final String SECRET_KEY = "RXOh9PoRU6Sp76vltxFLu5y6FlG6PNad";//自己的Secret Key
//
//    public static String sample(AipBodyAnalysis client,String type,String imagePath) {
//        // 传入可选参数调用接口
//        HashMap<String, String> options = new HashMap<String, String>();
//        options.put("type", type);
//
//
//        // 参数为本地路径
//        //图片的路径
//        JSONObject res = client.bodySeg(imagePath, options);
//        return res.get(type).toString();
//
//
//
//    }
//
//    /**
//     * base64字符串转换成图片 (对字节数组字符串进行Base64解码并生成图片)
//     * @param imgStr		base64字符串
//     * @param imgFilePath	指定图片存放路径  （注意：带文件名）
//     * @return
//     */
//    public static boolean Base64ToImage(String imgStr,String imgFilePath) {
//
//        if (StringUtils.isEmpty(imgStr)) // 图像数据为空
//            return false;
//
//        try {
//            // Base64解码
//            byte[] b = Base64Utils.decodeFromString(imgStr);
//            for (int i = 0; i < b.length; ++i) {
//                if (b[i] < 0) {// 调整异常数据
//                    b[i] += 256;
//                }
//            }
//
//            OutputStream out = new FileOutputStream(imgFilePath);
//            out.write(b);
//            out.flush();
//            out.close();
//
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//
//    }
//
//    /**
//     * 本地图片转换成base64字符串
//     * @param imgFile	本地图片全路径 （注意：带文件名）
//     *  (将图片文件转化为字节数组字符串，并对其进行Base64编码处理)
//     * @return
//     */
//    public static String ImageToBase64ByLocal(String imgFile) {
//
//
//        byte[] data = null;
//
//        // 读取图片字节数组
//        try {
//            InputStream in = new FileInputStream(imgFile);
//
//            data = new byte[in.available()];
//            in.read(data);
//            in.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // 返回Base64编码过的字节数组字符串
//        return Base64Utils.encodeToString(data);
//    }
//
//    /**
//     * base64字符串转化成图片
//     * @param imgStr 接口返回的图片base64数据
//     * @param imgFilePath 即将要保存的图片的本地路径包含文件名称和格式 例如:F:/generateimage.jpg
//     * @return
//     */
//    public static boolean GenerateImage(String imgStr, String imgFilePath) { // 对字节数组字符串进行Base64解码并生成图片
//        if (imgStr == null) // 图像数据为空
//            return false;
//        Base64.Decoder decoder = Base64.getDecoder();
//        try {
//            // Base64解码
//            byte[] b = decoder.decode(imgStr);
//            for (int i = 0; i < b.length; ++i) {
//                if (b[i] < 0) { // 调整异常数据
//                    b[i] += 256;
//                }
//            }
//            // 生成jpeg图片
//            OutputStream out = new FileOutputStream(imgFilePath); // 新生成的图片
//            out.write(b);
//            out.flush();
//            out.close();
//            System.out.println("保存成功" + imgFilePath);
//            return true;
//        } catch(Exception e) {
//            System.out.println("出错了" + e.getMessage());
//            return false;
//        }
//    }
//
//    public static void main(String[] args) {
//        String yuanshi="/Users/kyson/Downloads/WechatIMG118.jpeg";
//        String jieguo="/Users/kyson/Downloads/bbb.jpg";
//        AipBodyAnalysis c = new AipBodyAnalysis(Sample.APP_ID, Sample.API_KEY, Sample.SECRET_KEY);
//        String sam = Sample.sample(c,"foreground",yuanshi);
//        //把foreground 保存成png格式的透明图片
//        GenerateImage(sam, jieguo);
//        PngColoringUtil pngColoringUtil=new PngColoringUtil();
//        pngColoringUtil.changePNGBackgroudColor(jieguo,jieguo, Color.red);
//
//
//
//    }
//
//}
