package io.boot.common.utils;


import org.quartz.*;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.quartz.CronScheduleBuilder.cronSchedule;

/**
 * @ClassName QuartzUtil
 * @Description: Job管理中心
 * @author: wangxx
 * @since: 2017年12月1日00:00:33
 */
@Service
public class QuartzUtil {
    private static Scheduler sche;

    public QuartzUtil(Scheduler scheduler){
        sche = scheduler;
    }

    private final static String JOB_GROUP_NAME = "QUARTZ_JOBGROUP_NAME";
    //触发器组
    private final static String TRIGGER_GROUP_NAME = "QUARTZ_TRIGGERGROUP_NAME";

    /**
     * 添加任务的方法
     * @param jobName  任务名
     * @param triggerName  触发器名
     * @param jobClass  执行任务的类
     * @param seconds  间隔时间
     * @param data 需要采集主机的信息
     * @throws SchedulerException
     */
    public void addJob(String jobName, String triggerName, Class<? extends Job> jobClass, Date date, JobDataMap data) throws SchedulerException{

        //用于描叙Job实现类及其他的一些静态信息，构建一个作业实例
        JobDetail jobDetail = JobBuilder.newJob(jobClass)
                .withIdentity(jobName, JOB_GROUP_NAME)
                .setJobData(data)
                .build();
        //构建一个触发器，规定触发的规则
        Trigger trigger = TriggerBuilder.newTrigger()//创建一个新的TriggerBuilder来规范一个触发器
                .withIdentity(triggerName, TRIGGER_GROUP_NAME)//触发器名字和组名
                .startNow()//立即执行
                .withSchedule(
//                        SimpleScheduleBuilder.simpleSchedule()
//                                .withIntervalInMinutes(seconds)//时间间隔  单位：分钟
//                                .repeatForever()//一直执行
                        cronSchedule(getCron(date))
                )
                .build();//产生触发器

        //向Scheduler中添加job任务和trigger触发器
        sche.scheduleJob(jobDetail, trigger);
    }
    /**
     * 关闭触发器
     * @throws SchedulerException
     */
    public void stop() throws SchedulerException {
        if (null != this.sche) {
            this.sche.shutdown(true);
        }

    }

    public void removeJob(String jobName,String triggerName){
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, TRIGGER_GROUP_NAME);
            sche.pauseTrigger(triggerKey);// 停止触发器
            sche.unscheduleJob(triggerKey);// 移除触发器
            sche.deleteJob(JobKey.jobKey(jobName, JOB_GROUP_NAME));// 删除任务
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 启动Scheduler
     * @throws SchedulerException
     */
    public void start() throws SchedulerException{
        if(!sche.isShutdown()){
            this.sche.start();
        }
    }

    /***
     *
     * @param date 时间
     * @return  cron类型的日期
     */
    public static String getCron(final Date  date){
        SimpleDateFormat sdf = new SimpleDateFormat("ss mm HH dd MM ? yyyy");
        String formatTimeStr = "";
        if (date != null) {
            formatTimeStr = sdf.format(date);
        }
        return formatTimeStr;
    }

}
