package io.boot.common.mq;

import io.boot.config.RabbitConfig;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.amqp.core.Message;

import java.nio.charset.StandardCharsets;

/**
 * @author kyson
 * @date 2020/05/13 13:52
 */
@Component
public class MsgConsumer {

//    @RabbitListener(
//            bindings =
//                    {
//                            @QueueBinding(value = @Queue(value = RabbitConfig.TOPIC_QUEUE_NAME, durable = "true"),
//                                    exchange = @Exchange(value = RabbitConfig.TEST_TOPIC_EXCHANGE, type = "topic"),
//                                    key = RabbitConfig.TOPIC_ROUTINGKEY)
//                    })
//    @RabbitHandler
    public void processTopicMsg(Message massage) {
        String msg = new String(massage.getBody(), StandardCharsets.UTF_8);
    }

}
