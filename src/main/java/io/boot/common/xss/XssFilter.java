

package io.boot.common.xss;

import io.boot.common.exception.RRException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * XSS过滤
 *
 * @author kyson
 */
@Component
public class XssFilter implements Filter {

	public static String open;

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
		if("true".equals(open)) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			String method = httpServletRequest.getMethod();//获取请求方式
			String url = httpServletRequest.getRequestURI();//获取请求路	径
			if ("POST".equals(method) && !url.equals("/boot-fast/sys/logout") && !url.equals("/boot-fast/sys/login")) {
				throw new RRException("演示环境不能操作，如需了解联系我们！", 500);
			}
		}
		HttpServletRequest orgRequest = XssHttpServletRequestWrapper.getOrgRequest((HttpServletRequest) request);
		XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper(
				(HttpServletRequest) request);
		if(orgRequest.getParameterMap().get("svg_xml")!=null){
			chain.doFilter(request, response);
		}else {
			chain.doFilter(xssRequest, response);
		}
	}

	@Override
	public void destroy() {
	}

	@Value("${xss.open}")
	public void setActive(String open) {
		this.open = open;
	}
}
