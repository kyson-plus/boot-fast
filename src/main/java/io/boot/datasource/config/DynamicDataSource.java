/**
 * Copyright (c) 2018 kyson-boot All rights reserved.
 *
 * https://www.sz.io
 *
 * 版权所有，侵权必究！
 */

package io.boot.datasource.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 多数据源
 *
 * @author kyson
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicContextHolder.peek();
    }

}
