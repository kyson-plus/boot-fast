/**
 * Copyright (c) 2018 kyson-boot All rights reserved.
 *
 * https://www.sz.io
 *
 * 版权所有，侵权必究！
 */

package io.boot.datasource.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解
 *
 * @author kyson
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {
    String value() default "";
}
