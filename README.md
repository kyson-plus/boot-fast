**版本须知**
- v2.1 2020-05-14<br>
1.新增消息通知模块<br>
2.消息通知支持定时推送，立即推送两种<br>
3.消息推送支持离线推送，精准推送，保证消息可达性<br>
4.支持自定义推送部门，权限控制<br>


- v2.0 2020-05-06<br>
1.新增部门管理模块<br>
2.新增角色-部门数据权限模块<br>
3.全局新增BaseEntity hasAction字段可以确定是否是自己的数据<br>
4.注解@DataScope 开启数据权限查询<br>
5.@BaseData("save/update") 增加审计功能<br>


**项目说明** 
- boot-fast是一个轻量级的，前后端分离的Java快速开发平台，能快速开发项目并交付
- 支持MySQL、PostgreSQL等主流数据库
- 前端地址：https://gitee.com/kyson-plus/boot-fast-vue.git
- 演示地址：https://www.xiaobaibai.cn
- 默认账号密码： admin/admin
<br>
 

**具有如下特点** 
- 友好的代码结构及注释，便于阅读及二次开发
- 实现前后端分离，通过token进行数据交互，前端再也不用关注后端技术
- 灵活的权限控制，可控制到页面或按钮，满足绝大部分的权限需求
- 页面交互使用Vue2.x，极大的提高了开发效率
- 完善的代码生成机制，可在线生成entity、xml、dao、service、vue、sql代码，减少70%以上的开发任务
- 引入quartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能
- 引入API模板，根据token作为登录令牌，极大的方便了APP接口开发
- 引入Hibernate Validator校验框架，轻松实现后端校验
- 引入WebSocket通信协议，实现消息后台推送，在线推送
- 引入云存储服务，已支持：七牛云、阿里云、腾讯云等
- 引入swagger文档支持，方便编写API接口文档
- 引入Activiti工作流框架，直接上手，自定义用户，用户组。高度集成系统的用户，角色管理
<br> 

**项目结构** 
```
boot-fast
├─db  项目SQL语句
│
├─common 公共模块
│  ├─aspect 系统日志
│  ├─exception 异常处理
│  ├─validator 后台校验
│  └─xss XSS过滤
│ 
├─config 配置信息
│ 
├─modules 功能模块
│  ├─activiti 工作流模块
│  ├─app API接口模块(APP调用)
│  ├─job 定时任务模块
│  ├─monitor 监控模块
│  ├─msg 消息通知
│  ├─oss 文件服务模块
│  └─sys 权限模块
│ 
├─szApplication 项目启动类
│  
├──resources 
│  ├─mapper SQL对应的XML文件
│  └─static 静态资源

```
<br> 

**技术选型：** 
- 核心框架：Spring Boot 2.1
- 安全框架：Apache Shiro 1.4
- 视图框架：Spring MVC 5.0
- 工作流框架：Activiti 5.22.0
- 持久层框架：MyBatis 3.3
- 定时器：Quartz 2.3
- 数据库连接池：Druid 1.0
- 日志管理：SLF4J 1.7、Log4j
- 页面交互：Vue2.x 
<br> 


 **后端部署**
- 通过git下载源码
- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法
- 创建数据库sz_fast，数据库编码为UTF-8
- 执行db/mysql.sql文件，初始化数据
- 修改application-dev.yml，更新MySQL账号和密码
- Eclipse、IDEA运行szApplication.java，则可启动项目
- Swagger文档路径：http://localhost:8080/boot-fast/swagger/index.html
- Swagger注解路径：http://localhost:8080/boot-fast/swagger-ui.html



 **Activiti的后台是有数据库的支持，所有的表都以ACT_开头**

- act_re_: 'RE’表示repository。 这个前缀的表包含了流程定义和流程静态资源（图片，规则，等等）。<br>
- act_ru_: 'RU’表示runtime。 这些运行时的表，包含流程实例，任务，变量，异步任务，等运行中的数据。 Activiti只在流程实例执行过程中保存这些数据，在流程结束时就会删除这些记录。 这样运行时表可以一直很小速度很快。<br>
- act_id_: 'ID’表示identity。 这些表包含身份信息，比如用户，组等等。<br>
- act_hi_: 'HI’表示history。 这些表包含历史数据，比如历史流程实例，变量，任务等等。<br>
- act_ge_*: 通用数据，用于不同场景下，如存放资源文件。<br>




- 资源库流程规则表
>act_re_deployment 部署信息表<br>
act_re_model 流程设计模型部署表<br>
act_re_procdef 流程定义数据表<br>
- 运行时数据库表
>act_ru_execution 运行时流程执行实例表<br>
act_ru_identitylink 运行时流程人员表，主要存储任务节点与参与者的相关信息<br>
act_ru_task 运行时任务节点表<br>
act_ru_variable 运行时流程变量数据表<br>
- 历史数据库表
>act_hi_actinst 历史节点表<br>
act_hi_attachment 历史附件表<br>
act_hi_comment 历史意见表<br>
act_hi_identitylink 历史流程人员表<br>
act_hi_detail 历史详情表，提供历史变量的查询<br>
act_hi_procinst 历史流程实例表<br>
act_hi_taskinst 历史任务实例表<br>
act_hi_varinst 历史变量表<br>
- 组织机构表
>act_id_group 用户组信息表<br>
act_id_info 用户扩展信息表<br>
act_id_membership 用户与用户组对应信息表<br>
act_id_user 用户信息表<br>
这四张表很常见，基本的组织机构管理，关于用户认证方面建议还是自己开发一套，组件自带的功能太简单，使用中有很多需求难以满足<br>
通用数据表
act_ge_bytearray 二进制数据表<br>
act_ge_property 属性数据表存储整个流程引擎级别的数据,初始化表结构时，会默认插入三条记录<br>




- 个人联系方式 QQ:570901320


<br>
